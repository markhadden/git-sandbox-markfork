# Git Sandbox

A sandbox for working with Git code changes.

**Possible Agenda Items**

* Time tracking: MHRLRN-300-003	Learning, "Git Training" in comments.
* Highlight [Git Presentation](https://drive.google.com/drive/folders/1c1dYLSlzd8FpxMPWxDeLJvYq7U_F5G06)

Git Offline

* Initialize a repo
* Stage > Diff > Commit in VS Code
* Review commits in GitHub Desktop

Git Online

* Create empty remote and push files
* Save password
* Create empty remote and pull repo

Git Together

* Clone a shared repo
* Follow issues and commits
* Pull changes
* Stash
* Resolving conflicts

Git Workflows

* Fork 
* Branch
* Merging a fork or branch
* Suggesting changes to a repo you don't own: Fork > Branch > Pull Request > Delete Fork